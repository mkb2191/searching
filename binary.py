def binary_search(data, item):
    low = 0
    high = len(data) - 1
    count = 0
    while high - low > 0:
        mid = round((high + low)/2)
        count += 1
        if data[mid] >= item:
            count += 1
            if data[mid] > item:
                high = mid - 1
            else:
                return mid, count
        else:
            low = mid + 1
    return None, count
