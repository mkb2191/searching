def interpolation_search(data, item):
    '''
Based on binary search but instead of using the mid point it guesses
where the data should be based on how close it is to the highest/lowest value
of the area it is currently searching. It is designed for data with near
uniform distribution. For uniform data it is O(log(log(n))) on average, but
worst case it is O(n) compared to binary search with worst case of O(log n)
    '''
    low_pos = 0
    high_pos = len(data) - 1
    low = data[low_pos]
    high = data[high_pos]
    count = 0
    while high_pos - low_pos > 1 and high - low > 1:
        # estimate position of item assuming uniform distribution
        mid_pos = round(((item - low)/(high - low))* (high_pos - low_pos) + low_pos)
        count += 1
        if high_pos <= mid_pos:
            mid_pos = high_pos -1
        else:    
            count += 1
            if low_pos >= mid_pos:
                mid_pos = low_pos +1
        mid = data[mid_pos]
        count += 1
        if mid >= item:
            count += 1
            if mid > item:
                high_pos = mid_pos - 1
                high = data[high_pos]
            else:
                return mid_pos, count
        else:
            low_pos = mid_pos + 1
            low = data[low_pos]
    return None, count
