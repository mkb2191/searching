def linear_search(data, item):
    for pos, i in enumerate(data):
        if i == item:
            return pos, pos
    return None, len(data)

def linear_search_sorted(data, item):
    for pos, i in enumerate(data):
        if i >= item:
            if i == item:
                return pos, pos
            else:
                return None, pos
    return None, len(data)
