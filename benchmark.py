import random
import time

from linear import linear_search, linear_search_sorted
from binary import binary_search
from enhanced_binary import interpolation_search


METHODS = [
    [linear_search, 'linear_search_unsorted'],
    [linear_search_sorted, 'linear_search_sorted'],
    [binary_search, 'binary_search'],
    [interpolation_search, 'interpolation_search']
    ]
def benchmark_search(highest=1000, length=1000, repeat=1):
    scores = [[name, 0,0] for _, name in METHODS]
    last = 0
    for r in range(repeat):
        data = []
        for i in range(length):
            data.append(random.randint(0, highest))
        data.sort()
        for i in range(highest):
            in_data = i in data
            if time.time() - last > .5 or not in_data:
                print('item:%s in data: %s' % (i, in_data))
                last = time.time()
            for num, (method, name) in enumerate(METHODS):
                start = time.time()
                pos, count = method(data, i)
                scores[num][1] += count
                scores[num][2] += time.time() - start
                if pos is not None and data[pos] != i:
                    print(name, 'failed', i)
    print()
    print('Sorted by Operations:')
    for name, op, total_time in sorted(scores, key=lambda x: x[1]):
        print(name, ' '*(25 - len(name)), op)
    print()
    print('Sorted by Time:')
    for name, op, total_time in sorted(scores, key=lambda x: x[2]):
        print(name, ' '*(25 - len(name)), total_time)
                                        

benchmark_search(1000, 10000, 20)
